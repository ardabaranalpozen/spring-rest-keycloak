package com.springkeycloak.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "projects")
public class Project {
    private long id;
    private String projectName;
    private String projectOwner;
    private double projectPrice;

    public Project() {

    }

    public Project(String projectName, String projectOwner, double projectPrice) {
        this.projectName = projectName;
        this.projectOwner = projectOwner;
        this.projectPrice = projectPrice;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "project_name", nullable = false)
    public String getProjectName() {
        return projectName;
    }
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Column(name = "project_owner", nullable = false)
    public String getProjectOwner() {
        return projectOwner;
    }
    public void setProjectOwner(String projectOwner) {
        this.projectOwner = projectOwner;
    }

    @Column(name = "project_price", nullable = false)
    public double getProjectPrice() {
        return projectPrice;
    }
    public void setProjectPrice(double projectPrice) {
        this.projectPrice = projectPrice;
    }

    @Override
    public String toString() {
        return "Project [id=" + id + ", projectName=" + projectName + ", projectOwner=" + projectOwner + ", projectPrice=" + projectPrice
                + "]";
    }
}
