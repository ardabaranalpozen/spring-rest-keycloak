package com.springkeycloak.demo.controller;

import com.springkeycloak.demo.entity.Project;
import com.springkeycloak.demo.repository.ProjectRepository;
import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ProjectsController {
    @Autowired
    private ProjectRepository projectRepository;

    //@RolesAllowed({ "admin", "user" })
    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public List<Project> getProjects(@RequestHeader String Authorization) {
        return projectRepository.findAll();
    }

    //@RolesAllowed({ "admin", "user" })
    @RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
    public ResponseEntity<Project> getProject(@RequestHeader String Authorization,@PathVariable(value = "id") Long projectId)
            throws ResourceNotFoundException {
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project not found for this id :: " + projectId));
        return ResponseEntity.ok().body(project);
    }

    //@RolesAllowed({ "admin" })
    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public Project createProject(@RequestHeader String Authorization, @Valid @RequestBody Project project) {
        return projectRepository.save(project);
    }

    //@RolesAllowed({ "admin" })
    @RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE)
    public Map<String, Boolean> deleteProject(@RequestHeader String Authorization,@PathVariable(value = "id") Long projectId)
            throws ResourceNotFoundException {
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project not found for this id :: " + projectId));

        projectRepository.delete(project);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
